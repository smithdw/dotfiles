set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" Let Vundle manage itself
Plugin 'VundleVim/Vundle.vim'

" General Coding Plugins
Plugin 'Valloric/YouCompleteMe'
Plugin 'vim-airline/vim-airline'
Plugin 'scrooloose/syntastic'

" Easy YCM generator
Plugin 'rdnetto/YCM-Generator'

" Color schemes related
Plugin 'dracula/vim'
Plugin 'sheerun/vim-wombat-scheme'
call vundle#end()

set enc=utf-8
set fenc=utf-8
set termencoding=utf-8

set autoindent
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab

" Tabs are necessary for makefiles so remove that restriction
autocmd FileType make setlocal noexpandtab

set textwidth=120
set t_Co=256

syntax on
colorscheme wombat

set number
set showmatch
set mouse=a

" Intelligent comments
set comments=s1:/*,mb:\ *,elx:\ */

" Setup packages

" Doxygen Toolkit
let g:DoxygenToolKit_authorname="Dustin W. Smith"

" vim-airline
set laststatus=2

" Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list=1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

"-------------------------------------------------------------------
" Keyboard mappings
"------------------------------------------------------------------
" Save File
nmap <F2> :w<CR>
" Insert, Save, Insert
nmap <F3> <ESC>:w<CR>i
" Switch between header and source
map <F4> :e %:p:s,.h$,.X123X,:s,.cpp$,.h,:s,.X123X$,.cpp,<CR>
" Toggle spell-check
nmap <F5> :setlocal spell! spelllang=en_us<CR>
" Create Doxygen comment
map <F6> :Dox<CR>
" Build
map <F7> :make<CR>
" Clean Build
map <S-F7> :make clean all<CR>
" ?? I Don't know what this means
" goto definition with F12
map <F-12> <C-]>
" Not smart enough for the difference features yet
if &diff
    "diff settings
    map <M-Down> ]c
    map <M-Up> [c
    map <M-Left> do
    map <M-Right> dp
    map <F9> :new<CR>:read !svn diff<CR>:set syntax=diff buftype=nofile<CR>gg
else
    " spell settings
    :setlocal spell spelllang=en
    " set the spellfile - folder must exist
    set spellfile=~/.vim/spellfile.add
    map <M-Down> ]s
    map <M-Up> [s
endif
" Create a differnt color of background over 90 characters
execute "set colorcolumn=" . join(range(90,335), ',')
highlight Colorcolumn ctermbg=8

filetype plugin indent on


